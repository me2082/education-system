CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Usage  
 * Maintainers


INTRODUCTION
------------

A lecture on saving data in .txt files

   
REQUIREMENTS
------------

* Standard C++ VisualStudioCode
* HPS


INSTALLATION
------------

* install g++ and compile the code in the src folder

USAGE
------------

Everything in this is free to use by anyone.

MAINTAINERS
------------

rasmus.braunstein.jensen@dk.experis.com
