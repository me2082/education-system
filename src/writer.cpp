#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include "../hps/src/hps.h"
#include "writer.h"
#include "teacher.h"
using namespace std;

int writeFile(vector<pair<int, string>> dataToSend)
{

  vector<pair<int, string>> data;

  data = dataToSend;

  string serialized = hps::to_string(data);
  cout << "Writing " << serialized.size() << " Bytes" << endl;

  ofstream dataFile;
  dataFile.open("data.txt", ios::binary | ios::app);
  dataFile << serialized << endl;
  dataFile.close();
  return 0;
}