#include <iostream>
#include "person.h"
#include "teacher.h"
#include "writer.h"
#include "courses.h"
#include "reader.h"
using namespace std;

vector<pair<int, string>> newFile;

vector<pair<int, string>> saveFile;

int main()
{

    cout << "program run" << endl;

    // Creating a teacher
    Teacher teacher;
    teacher.setID(1);
    teacher.setName("Harry", "", "potter");
    teacher.setSalary(100);

    // Creating a student
    Student student;
    student.setID(2);
    student.setName("Ronald", "", "Mcfly");

    int iDToSend;

    // PackingData

    // Teacher
    pair<int, string> pair1 = {teacher.getID(), "Teacher"};
    pair<int, string> pair2 = {teacher.getID(), teacher.getFirstName()};
    pair<int, string> pair3 = {teacher.getID(), teacher.getMiddleName()};
    pair<int, string> pair4 = {teacher.getID(), teacher.getLastName()};
    pair<int, string> pair5 = {teacher.getID(), to_string(teacher.getSalary())};

    // Student
    pair<int, string> pair6 = {student.getID(), "Student"};
    pair<int, string> pair7 = {student.getID(), student.getFirstName()};
    pair<int, string> pair8 = {student.getID(), student.getMiddleName()};
    pair<int, string> pair9 = {student.getID(), student.getLastName()};

    // SavingData

    // Teacher
    newFile.push_back(pair1);
    newFile.push_back(pair2);
    newFile.push_back(pair3);
    newFile.push_back(pair4);
    newFile.push_back(pair5);

    // Student
    newFile.push_back(pair6);
    newFile.push_back(pair7);
    newFile.push_back(pair8);
    newFile.push_back(pair9);

    writeFile(newFile);

    saveFile = readFile();

    // Reading Data from file
    int j = 1;

    Teacher loadedTeacher;
    Student loadedStudent;

    for (int i = 0; i < saveFile.size(); i++)
    {
        // makes sure we dont confuse the different ID's
        if (saveFile[i].first >= j)
        {
            if (saveFile[i].second == "Teacher")
            {

                loadedTeacher.setID(saveFile[i].first);

                loadedTeacher.setName(saveFile[i + 1].second, saveFile[i + 2].second, saveFile[i + 3].second);

                loadedTeacher.setSalary(stoi(saveFile[i + 4].second));
            }
            if (saveFile[i].second == "Student")
            {
                Student loadedStudent;

                loadedStudent.setID(saveFile[i].first);

                loadedStudent.setName(saveFile[i + 1].second, saveFile[i + 2].second, saveFile[i + 3].second);
            }
        }
        j++;
    }

    cout << loadedTeacher.getID() << endl;
    cout << loadedTeacher.getName() << endl;
    cout << loadedTeacher.getSalary() << endl;

    cout << endl;

    cout << loadedStudent.getID() << endl;
    cout << loadedStudent.getName() << endl;
}
