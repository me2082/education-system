#include "person.h"

void Person::setName(string firstName, string middleName, string lastName)
{
    this->firstName = firstName;
    this->middleName = middleName;
    this->lastName = lastName;
}

string Person::getName()
{
    return firstName + " " + lastName;
}

void Person::setFirstName(string firstName)
{
    this->firstName = firstName;
}

string Person::getFirstName()
{
    return firstName;
}

void Person::setMiddleName(string middleName)
{
    this->middleName = middleName;
}

string Person::getMiddleName()
{
    return middleName;
}

void Person::setLastName(string lastName)
{
    this->lastName = lastName;
}

string Person::getLastName()
{
    return lastName;
}