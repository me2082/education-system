using namespace std;
#include <vector>
#include <iostream>
#include "teacher.h"
#include "student.h"

class Courses
{
protected:
    string courseName;
    string courseTopic;
    vector<Teacher> courseTeachers;
    vector<Student> courseStudents;
    //vector<Book> courseBooks;

public:
    void setName(string courseName);
    string getName();
    void setTopic(string courseTopic);
    string getTopic();
    void assignTeacher(Teacher courseTeachers);
    void removeTeacher(Teacher courseTeachers);
    vector<Teacher> getTeachers();
    void assignStudent(Student courseStudents);
    void removeStudent(Student courseStudents);
    vector<Student> getStudents(vector<Student> Students);
    //vector<Teacher> assignBook();
    //vector<Teacher> getBook();

};