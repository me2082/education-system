#pragma once
using namespace std;
#include <iostream>

class Person
{
protected:
    string firstName;
    string middleName;
    string lastName;

public:
    void setName(string firstName, string middleName, string lastName);
    string getName();

    void setFirstName(string firstName);
    string getFirstName();

    void setMiddleName(string middleName);
    string getMiddleName();

    void setLastName(string lastName);
    string getLastName();
};