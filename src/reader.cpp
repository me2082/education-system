#include <cassert>
#include <iostream>
#include <fstream>
#include "../hps/src/hps.h"
#include "reader.h"

using namespace std;

vector<pair<int, string>> readFile()
{
  vector<pair<int, string>> data;

  string line;

  ifstream dataFile;

  string filePath = "data.txt";

  dataFile.open(filePath, ios::binary);
  if(dataFile.is_open())
  {
    string dataRead;
    while(getline(dataFile, line))
    {
      // should only be one line to read!
      data = hps::from_string<vector<pair<int, string>>>(line);
    }
    dataFile.close();
  }
  else
  {
    cout << "File read error!" << endl;
  }

  /*for(pair<int, string> entry : data)
  {
    cout << entry.first << " | " << entry.second << std::endl;
  }*/

  return data;
}